<?php

header('Access-Control-Allow-Origin: *');

defined('BASEPATH') OR exit('No direct script access allowed');

class front extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function home() {
        $data = array();
        $data["categories"] = $this->list_categories();
        $this->load->view('template/header.php', $data);
        $this->load->view('home.php');
    }

    public function new_products($node) {
        $tab_all = array();
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "SearchIndex" => "FashionWomen",
            "ItemPage" => 1,
            "ResponseGroup" => "Images,ItemAttributes,Offers",
            "BrowseNode" => $node,
            "Sort" => "reviewrank"
        );

        $data = $this->send_request($params);

        $j = 1;
        foreach ($data->Items->Item as $item) {
            if ($j < 7) {
                $tab["asin"] = htmlspecialchars($item->ASIN);
                $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["imageHeight"] = htmlspecialchars($item->LargeImage->Height);
                $tab["imageWidth"] = htmlspecialchars($item->LargeImage->Width);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
                $tab_all[] = $tab;
                $j++;
            }
        }

        return $tab_all;
    }

    public function product_paginate() {
        $sort = $this->input->post('sort');
        $node = $this->input->post('node');
        $page = $this->input->post('page');
        $data = array();
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "SearchIndex" => "FashionWomen",
            "ItemPage" => $page,
            "ResponseGroup" => "Images,ItemAttributes,Offers",
            "BrowseNode" => $node,
            "Sort" => $sort
        );

        $data = $this->send_request($params);


        $j = 1;
        foreach ($data->Items->Item as $item) {
            if ($j < 10) {
                $tab["asin"] = htmlspecialchars($item->ASIN);
                $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["title"] = htmlspecialchars($item->ItemAttributes->Title);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["imageHeight"] = htmlspecialchars($item->LargeImage->Height);
                $tab["imageWidth"] = htmlspecialchars($item->LargeImage->Width);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
                $tab_all[] = $tab;
                $j++;
            }
        }

        $data = array();
        $data["tab"] = $tab_all;
        $this->load->view('search_list', $data);
    }

    public function product_list($node) {
        $data = array();
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "SearchIndex" => "FashionWomen",
            "ItemPage" => 1,
            "ResponseGroup" => "Images,ItemAttributes,Offers",
            "BrowseNode" => $node,
            "Sort" => "popularity-rank"
        );

        $data = $this->send_request($params);


        $j = 1;
        foreach ($data->Items->Item as $item) {
            if ($j < 10) {
                $tab["asin"] = htmlspecialchars($item->ASIN);
                $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["title"] = htmlspecialchars($item->ItemAttributes->Title);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["imageHeight"] = htmlspecialchars($item->LargeImage->Height);
                $tab["imageWidth"] = htmlspecialchars($item->LargeImage->Width);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
                $tab_all[] = $tab;
                $j++;
            }
        }

        return $tab_all;
    }
    
    public function product_info($asin) {
        
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemLookup",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "ItemId" => $asin,
            "IdType" => "ASIN",
            "ResponseGroup" => "Images,ItemAttributes"
        );
        
        $data = $this->send_request($params);


       $tab_f = array() ; 
        foreach ($data->Items->Item as $item) {
                $tab["asin"] = htmlspecialchars($item->ASIN);
               $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["title"] = htmlspecialchars($item->ItemAttributes->Title);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
        }
       

        return $tab;
    }

    public function product_sort() {
        $sort = $this->input->post('sort');
        $node = $this->input->post('node');
        $data = array();
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "SearchIndex" => "FashionWomen",
            "ItemPage" => 1,
            "ResponseGroup" => "Images,ItemAttributes,Offers",
            "BrowseNode" => $node,
            "Sort" => $sort
        );

        $data = $this->send_request($params);


        $j = 1;
        foreach ($data->Items->Item as $item) {
            if ($j < 10) {
                $tab["asin"] = htmlspecialchars($item->ASIN);
                $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["title"] = htmlspecialchars($item->ItemAttributes->Title);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["imageHeight"] = htmlspecialchars($item->LargeImage->Height);
                $tab["imageWidth"] = htmlspecialchars($item->LargeImage->Width);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
                $tab_all[] = $tab;
                $j++;
            }
        }
        $data = array();
        $data["tab"] = $tab_all;
        $this->load->view('search_list', $data);
    }

    public function load_products($node) {
        $categories = $this->list_categories() ; 
        $menu = $this->get_list($node);
        $title = ""; $key = "" ; 
        foreach ($categories as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    if ($v2 == $node) {
                        $title = $k1;
                        $child = $k2 ; 
                    }
                }
            }
        }
        $data = array();

        $data["title"] = $title;
        $data["child"] = $child;
        $data["node"] = $node;
        $data["menu"] = $menu ; 
        $data["tab_products"] = $this->product_list($node);
        $data["tab_selling"] = $this->new_products($node);
        $data["categories"] = $this->list_categories();
        $this->load->view('template/header.php', $data);
        $this->load->view('products');
    }
    
    public function show_product($asin) {
        $node  = 1040660 ; 
        $categories = $this->list_categories();
        $title = "";
        $tab_menu = "";
        foreach ($categories as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    if ($v2 == $node) {
                        $title = $k1;
                        $tab_menu = $v1;
                    }
                }
            }
        }
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemLookup",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "ItemId" => $asin,
            "IdType" => "ASIN",
            "ResponseGroup" => "Images,ItemAttributes,Offers,Reviews,SalesRank,Similarities"
        );
        
        $data = $this->send_request($params);


       $tab_f = array() ; 
        foreach ($data->Items->Item as $item) {
            
                foreach($item->ItemAttributes->Feature as $feature){
                    $tab_f[] = htmlspecialchars($feature) ; 
                }
                $x = 1 ;
                if($item->SimilarProducts->SimilarProduct != null){
            foreach ($item->SimilarProducts->SimilarProduct as $product) {
                if ($x < 4) {
                    $tab_s[] = $this->product_info(htmlspecialchars($product->ASIN));
                }
                $x++;
            }
                }else{
                    $tab_s = array();
                }
            foreach($item->ImageSets->ImageSet as $image){
                    $tab_i[] = htmlspecialchars($image->LargeImage->URL) ;
                }    
                $tab["asin"] = htmlspecialchars($item->ASIN);
                $tab["images"] = $tab_i ;
                $tab["products"] = $tab_s ;
                $tab["Features"] = $tab_f ;
                $tab["reviews"] = htmlspecialchars($item->CustomerReviews->IFrameURL);
                $tab["DetailPageURL"] = htmlspecialchars($item->DetailPageURL);
                $tab["title"] = htmlspecialchars($item->ItemAttributes->Title);
                $tab["model"] = htmlspecialchars($item->ItemAttributes->Model);
                $tab["departement"] = htmlspecialchars($item->ItemAttributes->Department);
                $tab["pub"] = htmlspecialchars($item->ItemAttributes->Publisher);
                $tab["manu"] = htmlspecialchars($item->ItemAttributes->Manufacturer);
                $tab["subTitle"] = $this->getWords(htmlspecialchars($item->ItemAttributes->Title));
                $tab["image"] = htmlspecialchars($item->LargeImage->URL);
                $tab["imageHeight"] = htmlspecialchars($item->LargeImage->Height);
                $tab["imageWidth"] = htmlspecialchars($item->LargeImage->Width);
                $tab["price"] = htmlspecialchars($item->ItemAttributes->ListPrice->FormattedPrice);
                $tab_all[] = $tab;
              
        }
        
        $data = array();

        $data["title"] = $title;
        $data["tab_menu"] = $tab_menu;
        $data["tab_product"] = $tab_all;
        $data["categories"] = $this->list_categories();
        $this->load->view('template/header.php', $data);
        $this->load->view('show');
    }
    
    public function get_list($node) {
        
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "BrowseNodeLookup",
            "AWSAccessKeyId" => "AKIAJ3W7IDZKAWYPMYDA",
            "AssociateTag" => "10d09-20",
            "BrowseNodeId" => $node,
            "ResponseGroup" => "BrowseNodeInfo"
        );


        $data = $this->send_request($params);


       $tab_f = array() ; 
        foreach ($data->BrowseNodes->BrowseNode->Children->BrowseNode as $browse) {
                $tab_f["name"] = htmlspecialchars($browse->Name);
                $tab_f["node"] = htmlspecialchars($browse->BrowseNodeId);
                $tab_all[] = $tab_f;
        }
        
        return $tab_all;
    }

    public function list_categories() {
        $tab = array(
            'Fashion' => array(
                'Women' => array(
                    'Clothing' => '1040660',
                    'Shoes'=>'679337011',
                    'Jewelry'=> '7192394011',
                    'Watches'=>'6358543011'
                ),
                'Men' => array(
                    'Clothing'=>'11307731011',
                    'Shoes'=>'679255011',
                    'Jewelry'=>'3887881',
                    'Watches'=>'6358539011',
                    'Accessories'=>'11307924011'
                ),
                'Baby' => array(
                    'Clothing',
                    'Shoes',
                    'Accessories'
                )
            ),
            'Electronics' => array(
                'Smartphones' => array(
                    'Apple',
                    'Samsung',
                    'LG',
                    'HTC',
                    'Sony',
                    'Huawei'
                ),
                'Computers & Tablets' => array(
                    'Laptops',
                    'Tablets',
                    'Desktops',
                    'Monitors',
                    'PC Gaming',
                    'Computer Accessories',
                    'PC Components'
                ),
                'Cameras & Photo' => array(
                    'Accessories',
                    'Bags & Cases',
                    'Digital Cameras',
                    'Flashes',
                    'Video',
                    'Underwater Photography'
                )
            ),
            'Motors' => array(
                'Vehicles' => array(
                    'Cars',
                    'Trucks',
                    'Vans'
                ),
                'Accessories' => array(
                    'Connected car',
                    'Electric vehicles',
                    'Your Garages'
                )
            )
        );

        return $tab;
    }

    public function getWords($inputstring) {
        $pieces = explode(" ", $inputstring);
        $first_part = implode(" ", array_splice($pieces, 0, 3));
        return $first_part;
    }

    public function send_request($params) {

        $aws_access_key_id = "AKIAJ3W7IDZKAWYPMYDA";

        $aws_secret_key = "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";


        $endpoint = "webservices.amazon.com";

        $uri = "/onca/xml";

        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        ksort($params);

        $pairs = array();

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }


        $canonical_query_string = join("&", $pairs);


        $string_to_sign = "GET\n" . $endpoint . "\n" . $uri . "\n" . $canonical_query_string;

        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));


        $request_url = 'http://' . $endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);


        $data = simplexml_load_file($request_url);

        return $data;
    }
   
}

<ul id="flexiselDemo3">
    <?php 
    foreach($tab as $key => $value){
    ?>
    <li><a href="<?php echo base_url()."show-product/".$value["asin"] ?>"><img src="<?php echo $value["image"] ?>" width="<?php echo $value["imageWidth"] ?>" height="<?php echo $value["imageHeight"] ?>" class="img-responsive"/></a>
        <div class="product liked-product simpleCart_shelfItem">
            <a class="like_name" href="<?php echo base_url()."show-product/".$value["asin"] ?>"><?php echo $value["subTitle"] ?></a>
            <p><a class="item_add" href="<?php echo base_url()."show-product/".$value["asin"] ?>"><i></i> <span class=" item_price"><?php echo $value["price"] ?></span></a></p>
        </div>
    </li>
    <?php 
    }
    ?>
</ul>
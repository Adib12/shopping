<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Eshop a Flat E-Commerce Bootstrap Responsive Website Template | Home :: w3layouts</title>
        <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Eshop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--webfont-->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- cart -->
        <script src="<?php echo base_url() ?>assets/js/simpleCart.min.js"></script>
        <!-- cart -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/flexslider.css" type="text/css" media="screen" />
    </head>
    <body>
        <!-- header-section-starts -->
        <div class="header">
            <div class="header-top-strip">
                <div class="container">
                    <div class="header-top-left">
                        <ul>
                            <li><a href="account.html"><span class="glyphicon glyphicon-user"> </span>Login</a></li>
                            <li><a href="register.html"><span class="glyphicon glyphicon-lock"> </span>Create an Account</a></li>			
                        </ul>
                    </div>
                    <div class="header-right">
                        <div class="cart box_1">
                            	
                             <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- header-section-ends -->
        <div class="banner-top">
            <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo">
                            <h1><a href="index.html"><span>E</span> -Shop</a></h1>
                        </div>
                    </div>
                    <!--/.navbar-header-->

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.html">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fashion<b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Women</h6>
                                                <li><a href="products.html">Clothing</a></li>
                                                <li><a href="products.html">Shoes</a></li>
                                                <li><a href="products.html">Jewelry</a></li>
                                                <li><a href="products.html">Watches</a></li>
                                                <li><a href="products.html">Handbags & Wallets</a></li>
                                                <li><a href="products.html">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Men</h6>
                                                <li><a href="products.html">Clothing</a></li>
                                                <li><a href="products.html">Shoes</a></li>
                                                <li><a href="products.html">Jewelry</a></li>
                                                <li><a href="products.html">Watches</a></li>
                                                <li><a href="products.html">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Baby</h6>
                                                <li><a href="products.html">Clothing</a></li>
                                                <li><a href="products.html">Shoes</a></li>
                                                <li><a href="products.html">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Electronics <b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Smartphones</h6>
                                                <li><a href="products.html">Apple</a></li>
                                                <li><a href="products.html">Samsung</a></li>
                                                <li><a href="products.html">LG</a></li>
                                                <li><a href="products.html">HTC</a></li>
                                                <li><a href="products.html">Sony</a></li>
                                                <li><a href="products.html">Huawei</a></li>
                                                <li><a href="products.html">BlackBerry</a></li>
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Cameras & Photo</h6>
                                                <li><a href="products.html">Accessories</a></li>
                                                <li><a href="products.html">Bags & Cases</a></li>
                                                <li><a href="products.html">Binoculars & Scopes</a></li>
                                                <li><a href="products.html">Digital Cameras</a></li>
                                                <li><a href="products.html">Flashes</a></li>
                                                <li><a href="products.html">Video</a></li>
                                                <li><a href="products.html">Projectors</a></li>
                                                <li><a href="products.html">Underwater Photography</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Computers & Tablets</h6>
                                                <li><a href="products.html">Laptops</a></li>
                                                <li><a href="products.html">Tablets</a></li>
                                                <li><a href="products.html">Desktops</a></li>
                                                <li><a href="products.html">Monitors</a></li>
                                                <li><a href="products.html">PC Gaming</a></li>
                                                <li><a href="products.html">Computer Accessories</a></li>
                                                <li><a href="products.html">PC Components</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sports <b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Smartphones</h6>
                                                <li><a href="products.html">Apple</a></li>
                                                <li><a href="products.html">Samsung</a></li>
                                                <li><a href="products.html">LG</a></li>
                                                <li><a href="products.html">HTC</a></li>
                                                <li><a href="products.html">Sony</a></li>
                                                <li><a href="products.html">Huawei</a></li>
                                                <li><a href="products.html">BlackBerry</a></li>
                                                
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Cameras & Photo</h6>
                                                <li><a href="products.html">Accessories</a></li>
                                                <li><a href="products.html">Bags & Cases</a></li>
                                                <li><a href="products.html">Binoculars & Scopes</a></li>
                                                <li><a href="products.html">Digital Cameras</a></li>
                                                <li><a href="products.html">Flashes</a></li>
                                                <li><a href="products.html">Video</a></li>
                                                <li><a href="products.html">Projectors</a></li>
                                                <li><a href="products.html">Underwater Photography</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="multi-column-dropdown">
                                                <h6>Computers & Tablets</h6>
                                                <li><a href="products.html">Laptops</a></li>
                                                <li><a href="products.html">Tablets</a></li>
                                                <li><a href="products.html">Desktops</a></li>
                                                <li><a href="products.html">Monitors</a></li>
                                                <li><a href="products.html">PC Gaming</a></li>
                                                <li><a href="products.html">Computer Accessories</a></li>
                                                <li><a href="products.html">PC Components</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">kids <b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-2">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <ul class="multi-column-dropdown">
                                                <h6>NEW IN</h6>
                                                <li><a href="products.html">New In Boys Clothing</a></li>
                                                <li><a href="products.html">New In Girls Clothing</a></li>
                                                <li><a href="products.html">New In Boys Shoes</a></li>
                                                <li><a href="products.html">New In Girls Shoes</a></li>
                                            </ul>
                                        </div>
                                      
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>
                            <li><a href="contact.html">CONTACT</a></li>
                        </ul>
                    </div>
                    <!--/.navbar-collapse-->
                </nav>
                <!--/.navbar-->
            </div>
        </div>
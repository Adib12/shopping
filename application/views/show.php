<!-- content-section-starts -->
<div class="container">
    <div class="products-page">
        <div class="products">
            <div class="product-listy">
                <h2>
                    <?php echo $title ?>
                </h2>
                <ul class="product-list">
                    <?php
                    foreach ($tab_menu as $kc => $vc) {
                        ?>
                        <li><a href="<?php echo base_url() . "products/" . $vc?>"><?php echo $kc; ?></a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>


        </div>
    <?php 
            foreach($tab_product as $key => $value){
            ?>
        <div class="new-product">
            <div class="col-md-5 zoom-grid">
                <div class="flexslider">
                    <ul class="slides">
                         <li data-thumb="<?php echo $value["image"] ?>">
                            <div class="thumb-image"> <img src="<?php echo $value["image"] ?>" data-imagezoom="true" class="img-responsive" alt="" /> </div>
                        </li>
                        <?php 
                        foreach($value["images"] as $image){
                        ?>
                        <li data-thumb="<?php echo $image ?>">
                            <div class="thumb-image"> <img src="<?php echo $image ?>" data-imagezoom="true" class="img-responsive" alt="" /> </div>
                        </li>
                        <?php 
                        }
                        ?>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-7 dress-info">
                <div class="dress-name">
                    <h3><?php echo $value["subTitle"] ?></h3>
                    <span><?php echo $value["price"] ?></span>
                    <div class="clearfix"></div>
                    <p>
                        <?php echo $value["title"] ?>
                    </p>
                   
                </div>
                 <div class="span span1">
                    <p class="left">Departement</p>
                    <p class="right"><?php echo $value["departement"] ?></p>
                    <div class="clearfix"></div>
                </div>
                
                <div class="span span2">
                    <p class="left">Model</p>
                    <p class="right"><?php echo $value["model"] ?></p>
                    <div class="clearfix"></div>
                </div>
                <div class="span span3">
                    <p class="left">Manufacturer</p>
                    <p class="right"><?php echo $value["manu"] ?></p>
                    <div class="clearfix"></div>
                </div>
                <div class="span span4">
                    <p class="left">Publisher</p>
                    <p class="right"><?php echo $value["pub"] ?></p>
                    <div class="clearfix"></div>
                </div>
                
              
                <div class="purchase">
                    <a href="<?php echo $value["DetailPageURL"] ?>" target="_blank">Shop Now</a>
                   
                    <div class="clearfix"></div>
                </div>
                <script src="<?php echo base_url() ?>assets/js/imagezoom.js"></script>
                <!-- FlexSlider -->
                <script defer src="<?php echo base_url() ?>assets/js/jquery.flexslider.js"></script>
                <script>
                    // Can also be used with $(document).ready()
                    $(window).load(function () {
                        $('.flexslider').flexslider({
                            animation: "slide",
                            controlNav: "thumbnails"
                        });
                    });
                </script>
            </div>
           
            <div class="clearfix"></div>
            <div class="reviews-tabs">
                <!-- Main component for a primary marketing message or call to action -->
                <ul class="nav nav-tabs responsive hidden-xs hidden-sm" id="myTab">
                    <li class="test-class active"><a class="deco-none misc-class" href="#how-to">More Information</a></li>
                     </ul>

                <div class="tab-content responsive hidden-xs hidden-sm">
                    <div class="tab-pane active" id="how-to">
                        <p class="tab-text">
                           <?php 
                            foreach($value["Features"] as $feature){
                                echo $feature."<br>" ;
                            }
                        ?>
                        </p>    
                    </div>
                </div>		
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
</div>
     <?php } ?>
<div class="other-products products-grid">
    <div class="container">
        <?php 
        if(!empty($value["products"])){
        ?>
        <header>
            <h3 class="like text-center">Related Products</h3>   
        </header>
        
        <?php 
        foreach($value["products"] as $kk => $v){
        ?>
        <div class="col-md-4 product simpleCart_shelfItem text-center">
            <a href="single.html"><img src="<?php echo $v["image"] ?>" /></a>
            <div class="mask">
                <a href="single.html">Quick View</a>
            </div>
            <a class="product_name" href="single.html"><?php echo $v["subTitle"] ?></a>
            <p><a class="item_add" href="#"><i></i> <span class="item_price"><?php echo $v["price"] ?></span></a></p>
        </div>
        <?php
        } }
        ?>
        
        <div class="clearfix"></div>
    </div>
</div>
<!-- content-section-ends -->
<div class="news-letter">
    <div class="container">
        <div class="join">
            <h6>JOIN OUR MAILING LIST</h6>
            <div class="sub-left-right">
                <form>
                    <input type="text" value="Enter Your Email Here" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                                    this.value = 'Enter Your Email Here';}" />
                    <input type="submit" value="SUBSCRIBE" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="span_of_4">
                <div class="col-md-3 span1_of_4">
                    <h4>Shop</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                    </ul>	
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>help</h4>
                    <ul class="f_nav">
                        <li><a href="#">frequently asked  questions</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>	
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>account</h4>
                    <ul class="f_nav">
                        <li><a href="account.html">login</a></li>
                        <li><a href="register.html">create an account</a></li>
                        <li><a href="#">create wishlist</a></li>
                        <li><a href="checkout.html">my shopping bag</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">create wishlist</a></li>
                    </ul>					
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>popular</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                        <li><a href="#">login</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>			
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="cards text-center">
            <img src="<?php echo base_url() ?>assets/images/cards.jpg" alt="" />
        </div>
        <div class="copyright text-center">
            <p>© 2015 Eshop. All Rights Reserved | Design by   <a href="http://w3layouts.com">  W3layouts</a></p>
        </div>
    </div>
</div>
</body>
</html>

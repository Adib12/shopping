<!-- content-section-starts -->
<div class="container">
    <div class="products-page">
        <div class="products">
            <div class="product-listy">
                <h2>
                    <?php echo $title." / ".$child ?>
                </h2>
                <ul class="product-list">
                    <?php
                   
                    
                    foreach ($menu as $kc => $vc) {
                        ?>
                        <li><a href="<?php echo base_url() . "products/" . $vc["node"] ?>"><?php echo $vc["name"]; ?></a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>


        </div>
        <div class="new-product">
            <div class="new-product-top">
                <ul class="product-top-list">
                    <li><a href="index.html">Home</a>&nbsp;<span>&gt;</span></li>
                    <li><span class="act">Products</span>&nbsp;</li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="mens-toolbar">
                <div class="sort">
                    <div class="sort-by">
                        <label>Sort By</label>
                        <select id="sortV">
                            <option value="popularity-rank">Popularity </option>
                            <option value="price">Price </option>
                            <option value="launch-date">Launch date </option>
                        </select>
                    </div>
                </div>
                <ul class="women_pagenation">
                    <li>Page:</li>
                    <?php 
                    for($i=1;$i<11;$i++){
                        if($i==1){
                            ?>
                        
                             <li id="<?php echo "p".$i ?>" class="active"><a class="fake-link" data-href="<?php echo $i ?>"><?php echo $i ?></a></li>
                   <?php 
                        }else{
                            ?>
                             <li id="<?php echo "p".$i ?>" ><a class="fake-link" data-href="<?php echo $i ?>"><?php echo $i ?></a></li>
                         <?php 
                    }
                    }
                    ?>
                    
                    
                </ul>
                <div class="clearfix"></div>		
            </div>
            <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                <div class="cbp-vm-options">
                    <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid" title="grid">Grid View</a>
                    <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list" title="list">List View</a>
                </div>

                <div class="clearfix"></div>
                <div id="productContainer">
                    <ul id="productList">             
                        <?php
                        foreach ($tab_products as $key => $value) {
                            ?>
                            <li>
                                <a class="cbp-vm-image" href="<?php echo base_url()."show-product/".$value["asin"] ?>">
                                    <div class="simpleCart_shelfItem">
                                        <div class="view view-first">
                                            <div class="inner_content clearfix">
                                                <div class="product_image">
                                                    <img src="<?php echo $value["image"] ?>" width="<?php echo $value["imageWidth"] ?>" height="<?php echo $value["imageHeight"] ?>" class="img-responsive" alt=""/>
                                                    <div class="mask">
                                                        <div class="info">Quick View</div>
                                                    </div>
                                                    <div class="product_container">
                                                        <div class="cart-left">
                                                            <p class="title"><?php echo $value["subTitle"] ?></p>
                                                        </div>
                                                        <div class="pricey"><span class="item_price"><?php echo $value["price"] ?></span></div>
                                                        <div class="clearfix"></div>
                                                    </div>		
                                                </div>
                                            </div>
                                        </div>
                                </a>
                                <div class="cbp-vm-details">
                                    <?php echo $value["title"] ?>
                                </div>
                                <a class="cbp-vm-icon cbp-vm-add item_add" href="<?php echo $value["DetailPageURL"] ?>" target="_blank">Shop Now</a>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>  
                </div>


            </div>
            <script src="<?php echo base_url() ?>assets/js/cbpViewModeSwitch.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>assets/js/classie.js" type="text/javascript"></script>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- content-section-ends -->
<div class="other-products">
    <div class="container">
        <h3 class="like text-center">Most selling products </h3>  

        <ul id="flexiselDemo3">
            <?php
            foreach ($tab_selling as $key => $value) {
                ?>
                <li><a href="<?php echo base_url()."show-product/".$value["asin"] ?>"><img src="<?php echo $value["image"] ?>" width="<?php echo $value["imageWidth"] ?>" height="<?php echo $value["imageHeight"] ?>" class="img-responsive"/></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="<?php echo base_url()."show-product/".$value["asin"] ?>"><?php echo $value["subTitle"] ?></a>
                        <p><a class="item_add" href="<?php echo base_url()."show-product/".$value["asin"] ?>"><i></i> <span class=" item_price"><?php echo $value["price"] ?></span></a></p>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
        <script type="text/javascript">
            $(window).load(function () {
                $("#flexiselDemo3").flexisel({
                    visibleItems: 4,
                    animationSpeed: 1000,
                    autoPlay: true,
                    autoPlaySpeed: 3000,
                    pauseOnHover: true,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 480,
                            visibleItems: 1
                        },
                        landscape: {
                            changePoint: 640,
                            visibleItems: 2
                        },
                        tablet: {
                            changePoint: 768,
                            visibleItems: 3
                        }
                    }
                });

            });
        </script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.flexisel.js"></script>
    </div>
</div>
<!-- content-section-ends-here -->
<div class="news-letter">
    <div class="container">
        <div class="join">
            <h6>JOIN OUR MAILING LIST</h6>
            <div class="sub-left-right">
                <form>
                    <input type="text" value="Enter Your Email Here" onfocus="this.value = '';" onblur="if (this.value == '') {
                                this.value = 'Enter Your Email Here';
                            }" />
                    <input type="submit" value="SUBSCRIBE" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="span_of_4">
                <div class="col-md-3 span1_of_4">
                    <h4>Shop</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                    </ul>	
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>help</h4>
                    <ul class="f_nav">
                        <li><a href="#">frequently asked  questions</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>	
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>account</h4>
                    <ul class="f_nav">
                        <li><a href="account.html">login</a></li>
                        <li><a href="register.html">create an account</a></li>
                        <li><a href="#">create wishlist</a></li>
                        <li><a href="checkout.html">my shopping bag</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">create wishlist</a></li>
                    </ul>					
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>popular</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                        <li><a href="#">login</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>			
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="cards text-center">
            <img src="<?php echo base_url() ?>assets/images/cards.jpg" alt="" />
        </div>
        <div class="copyright text-center">
            <p>© 2015 Eshop. All Rights Reserved | Design by   <a href="http://w3layouts.com">  W3layouts</a></p>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var node = "<?php echo $node ?>";
        var sort = $("#sortV").val() ;
        
        var source = "<?php echo base_url()."assets/images/load.gif" ?>" ; 
         $(".fake-link").click(function () {
             var sort1 = $("#sortV").val() ;
             $("#productContainer").html('<br><br><center><img src="'+source+'"></center>');
             var page = $(this).attr('data-href') ; 
             $.post("<?php echo base_url() . "Front/product_paginate" ?>", {page: page, node: node, sort: sort1}, function (data) {
               
                  $("#productContainer").html(data);
            });
             
         });
         $('ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
        $("#sortV").change(function () {
            $('ul li.active').removeClass('active');
    $("#p1").addClass('active');
             $("#productContainer").html('<br><br><center><img src="'+source+'"></center>');
            var sort = $("#sortV").val() ; 
            $.post("<?php echo base_url() . "Front/product_sort" ?>", {node: node, sort: sort}, function (data) {
                $("#productContainer").html(data);
            });
            
        });

        /*
         $.post("<?php echo base_url() . "Front/best_selling" ?>",  {node: node} , function (data) {
         $("#mostSellingContainer").html(data);
         });
         */
    });
</script>
</body>
</html>
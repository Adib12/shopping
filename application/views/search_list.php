<ul id="productList">             
    <?php
    foreach ($tab as $key => $value) {
            ?>
            <li>
                <a class="cbp-vm-image" href="<?php echo base_url()."show-product/".$value["asin"] ?>">
                    <div class="simpleCart_shelfItem">
                        <div class="view view-first">
                            <div class="inner_content clearfix">
                                <div class="product_image">
                                    <img src="<?php echo $value["image"] ?>" width="<?php echo $value["imageWidth"] ?>" height="<?php echo $value["imageHeight"] ?>" class="img-responsive" alt=""/>
                                    <div class="mask">
                                        <div class="info">Quick View</div>
                                    </div>
                                    <div class="product_container">
                                        <div class="cart-left">
                                            <p class="title"><?php echo $value["subTitle"] ?></p>
                                        </div>
                                        <div class="pricey"><span class="item_price"><?php echo $value["price"] ?></span></div>
                                        <div class="clearfix"></div>
                                    </div>		
                                </div>
                            </div>
                        </div>
                </a>
                <div class="cbp-vm-details">
                    <?php echo $value["title"] ?>
                </div>
                <a class="cbp-vm-icon cbp-vm-add item_add" href="<?php echo $value["DetailPageURL"] ?>" target="_blank">Shop Now</a>
                </div>
            </li>
            <?php
        }
    ?>
</ul>